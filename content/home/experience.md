---
# An instance of the Experience widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: experience

# Activate this widget? true/false
active: true
# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 20

title: Experience
subtitle:

# Date format for experience
#   Refer to https://wowchemy.com/docs/customization/#date-format
date_format: Jan 2006

# Experiences.
#   Add/remove as many `experience` items below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin multi-line descriptions with YAML's `|2-` multi-line prefix.
experience:
  - title: Staff Machine Learning Engineer
    company: The Estée Lauder Companies
    company_url: 'https://www.elcompanies.com/en'
    company_logo: elc_logo
    location: New York City
    date_start: '2022-11-14'
    date_end: ''
    # description: |2-
        # Responsibilities include:
        # 
        # * Analysing
        # * Modelling
        # * Deploying

design:
  columns: '2'
---
