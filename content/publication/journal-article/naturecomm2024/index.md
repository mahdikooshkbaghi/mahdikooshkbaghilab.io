---
title: "Specificity, synergy, and mechanisms of splice-modifying drugs"
authors:
- Yuma Ishigami
- Mandy S. Wong
- Carlos Martí-Gómez
- Andalus Ayaz
- admin
- Sonya M. Hanson
- David M. McCandlish
- Adrian R. Krainer
- Justin B. Kinney

date: "2024-01-29T00:00:00Z"
doi: "https://doi.org/10.1038/s41467-024-46090-5"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Nature Communications 15, 1880 (2024)"
publication_short: ""

abstract: Drugs that target pre-mRNA splicing hold great therapeutic potential, but the quantitative understanding of how these drugs work is limited. Here we introduce mechanistically interpretable quantitative models for the sequence-specific and concentration-dependent behavior of splice-modifying drugs. Using massively parallel splicing assays, RNA-seq experiments, and precision dose-response curves, we obtain quantitative models for two small-molecule drugs, risdiplam and branaplam, developed for treating spinal muscular atrophy. The results quantitatively characterize the specificities of risdiplam and branaplam for 5’ splice site sequences, suggest that branaplam recognizes 5’ splice sites via two distinct interaction modes, and contradict the prevailing two-site hypothesis for risdiplam activity at SMN2 exon 7. The results also show that anomalous single-drug cooperativity, as well as multi-drug synergy, are widespread among small-molecule drugs and antisense-oligonucleotide drugs that promote exon inclusion. Our quantitative models thus clarify the mechanisms of existing treatments and provide a basis for the rational development of new therapies.
# Summary. An optional shortened abstract.
tags:
- Computational Biology
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/naturecomm2024.pdf
url_code: 'https://doi.org/10.5281/zenodo.8353692'
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
