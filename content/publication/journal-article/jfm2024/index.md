---
title: "Characterization of partial wetting by CMAS droplets using multiphase many-body dissipative particle dynamics and data-driven discovery based on PINNs"
authors:
- Elham Kiyani
- admin
- Khemraj Shukla
- Rahul Babu Koneru
- Zhen Li
- Luis Bravo
- Anindya Ghoshal
- George Em Karniadakis
- Mikko Karttunen
date: "2024-04-01T00:00:00Z"
doi: "https://doi.org/10.1017/jfm.2024.270"

publication_types: ["2"]

publication: "Journal of Fluid Mechanics , Volume 985, A7"
publication_short: ""

abstract: The molten sand that is a mixture of calcia, magnesia, alumina and silicate, known as CMAS, is characterized by its high viscosity, density and surface tension. The unique properties of CMAS make it a challenging material to deal with in high-temperature applications, requiring innovative solutions and materials to prevent its buildup and damage to critical equipment. Here, we use multiphase many-body dissipative particle dynamics simulations to study the wetting dynamics of highly viscous molten CMAS droplets. The simulations are performed in three dimensions, with varying initial droplet sizes and equilibrium contact angles. We propose a parametric ordinary differential equation (ODE) that captures the spreading radius behaviour of the CMAS droplets. The ODE parameters are then identified based on the physics-informed neural network (PINN) framework. Subsequently, the closed-form dependency of parameter values found by the PINN on the initial radii and contact angles are given using symbolic regression. Finally, we employ Bayesian PINNs (B-PINNs) to assess and quantify the uncertainty associated with the discovered parameters. In brief, this study provides insight into spreading dynamics of CMAS droplets by fusing simple parametric ODE modelling and state-of-the-art machine-learning techniques.

# Summary. An optional shortened abstract.
tags:
- Machine Learning
- Fluid Mechanics
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/jfm2024.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
