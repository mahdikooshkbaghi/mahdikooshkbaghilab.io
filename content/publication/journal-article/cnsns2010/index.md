---
title: " A similarity solution in order to solve the governing equations of laminar separated fluids with a flat plate"
authors:
- Hossein Shokouhmand
- Muhammad Fakoor Pakdaman
- admin
date: "2010-12-01T00:00:00Z"
doi: "https://doi.org/10.1016/j.cnsns.2010.01.031"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Communications in Nonlinear Science and Numerical Simulation 15, no. 12 (2010): 3965-3973"
publication_short: ""

abstract: In this paper, the development of local and non-local similarity solutions for laminar flow and heat transfer between two separated fluids is described. This paper focuses on the extension of similarity solutions of convective surface boundary condition. This new case represents two adjacent fluids separated by a flat plate, and moving parallel to each other, where the convective heat transfer coefficient of the fluid heating the plate on its lower surface is proportional to . Numerical and analytical solutions are available to solve this boundary value problem. Within the first case, shooting method is applied; furthermore, Runge–Kutta fourth order method is used for integration over the whole boundary layer. Numerical solutions of the resulting similarity energy equation are represented for various Prandtl numbers and a range of values of the parameter characterizing the hot fluid convection process. In addition, analytical exact series solutions are provided for all different Prandtl numbers, although for cold fluids with low Prandtl numbers, a compact solution is also obtained. Finally, an appropriate range of Prandtl number is obtained in which compact and exact solution have a good agreement.

# Summary. An optional shortened abstract.
tags:
- Fluid Mechanics
- Model Reduction
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/cnsns2010.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
