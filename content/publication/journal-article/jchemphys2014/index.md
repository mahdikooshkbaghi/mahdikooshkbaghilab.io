---
title: "The global relaxation redistribution method for reduction of combustion kinetics"
authors:
- admin
- Christos E. Frouzakis
- Eliodoro Chiavazzo
- Konstantinos Boulouchos
- Ilya V. Karlin
date: "2014-07-01T00:00:00Z"
doi: "https://doi.org/10.1063/1.4890368"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "The Journal of chemical physics 141, no. 4 (2014): 044102"
publication_short: ""

abstract: An algorithm based on the Relaxation Redistribution Method (RRM) is proposed for constructing the Slow Invariant Manifold (SIM) of a chosen dimension to cover a large fraction of the admissible composition space that includes the equilibrium and initial states. The manifold boundaries are determined with the help of the Rate Controlled Constrained Equilibrium method, which also provides the initial guess for the SIM. The latter is iteratively refined until convergence and the converged manifold is tabulated. A criterion based on the departure from invariance is proposed to find the region over which the reduced description is valid. The global realization of the RRM algorithm is applied to constant pressure auto-ignition and adiabatic premixed laminar flames of hydrogen-air mixtures.

# Summary. An optional shortened abstract.
tags:
- Combustion
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/jchemphys2014.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
