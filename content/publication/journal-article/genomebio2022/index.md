---
title: "MAVE-NN: learning genotype-phenotype maps from multiplex assays of variant effect"
authors:
- Ammar Tareen
- admin
- Anna Posfai
- William T. Ireland
- David M. McCandlish
- Justin B. Kinney
date: "2022-04-15T00:00:00Z"
doi: "https://doi.org/10.1186/s13059-022-02661-7"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Genome Biology 23, 98 (2022)"
publication_short: ""

abstract: Multiplex assays of variant effect (MAVEs) are a family of methods that includes deep mutational scanning experiments on proteins and massively parallel reporter assays on gene regulatory sequences. Despite their increasing popularity, a general strategy for inferring quantitative models of genotype-phenotype maps from MAVE data is lacking. Here we introduce MAVE-NN, a neural-network-based Python package that implements a broadly applicable information-theoretic framework for learning genotype-phenotype mapsfrom MAVE datasets. We demonstrate MAVE-NN in multiple biological contexts, and highlight the ability of our approach to deconvolve mutational effects from otherwise confounding experimental nonlinearities and noise.

# Summary. An optional shortened abstract.
tags:
- Computational Biology
- Machine Learning
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/genomebio2022.pdf
url_code: 'https://github.com/jbkinney/mavenn'
url_dataset: ''
url_poster: ''
url_project: 'https://mavenn.readthedocs.io/en/latest/'
url_slides: ''
url_source: ''
url_video: ''
---
