---
title: "On the model reduction for chemical and physical kinetics"
authors:
- admin 
date: "2015-01-01T00:00:00Z"
doi: "https://doi.org/10.3929/ethz-a-010556318"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: "ETH Zurich"
publication_short: ""

abstract: The need to design of efficient combustion systems with minimal emissions of pollutants has led to the development of large detailed reaction mechanisms for combustion involving hundreds of chemical species reacting in a complex network of thousands of elementary reactions. Incorporating such a detailed reaction mechanism into multidimensional simulations is practically impossible. Different methodologies have been proposed for the reduction of detailed mechanisms. In the present work, model reduction approaches based on timescale separation and thermodynamic analysis are revisited, introduced, validated and used.

tags:
- Physical Kinetics
- Combustion
- Dynamical Systems
- Model Reduction 
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/phdthesis.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: uploads/phd_presentation.pdf
url_source: ''
url_video: ''
---
