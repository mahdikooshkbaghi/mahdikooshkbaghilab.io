---
title: "Manifold learning for organizing unstructured sets of process observations"
authors:
- Felix Dietrich
- admin
- Erik M. Bolt
- Ioannis G. Kevrekidis
date: "2020-04-01T00:00:00Z"
doi: "https://doi.org/10.1063/1.5133725"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Chaos: An Interdisciplinary Journal of Nonlinear Science 30, no. 4 (2020): 043108"
publication_short: ""

abstract: Data mining is routinely used to organize ensembles of short temporal observations so as to reconstruct useful, low-dimensional realizations of an underlying dynamical system. In this paper, we use manifold learning to organize unstructured ensembles of observations (“trials”) of a system’s response surface. We have no control over where every trial starts, and during each trial, operating conditions are varied by turning “agnostic” knobs, which change system parameters in a systematic, but unknown way. As one (or more) knobs “turn,” we record (possibly partial) observations of the system response. We demonstrate how such partial and disorganized observation ensembles can be integrated into coherent response surfaces whose dimension and parametrization can be systematically recovered in a data-driven fashion. The approach can be justified through the Whitney and Takens embedding theorems, allowing reconstruction of manifolds/attractors through different types of observations. We demonstrate our approach by organizing unstructured observations of response surfaces, including the reconstruction of a cusp bifurcation surface for hydrogen combustion in a continuous stirred tank reactor. Finally, we demonstrate how this observation-based reconstruction naturally leads to informative transport maps between the input parameter space and output/state variable spaces.

# Summary. An optional shortened abstract.
tags:
- Machine Learning
- Dynamical Systems
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/chaos2020b.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
