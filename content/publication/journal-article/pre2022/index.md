---
title: "Machine-learning-based data-driven discovery of nonlinear phase-field dynamics"
authors:
- Elham Kiyani
- Steven Silber
- admin 
- Mikko Karttunen
date: "2022-12-06T00:00:00Z"
doi: "https://doi.org/10.1103/PhysRevE.106.065303"
publication_types: ["2"]

publication: "Physical Review E 106 (065303) (2022)"
publication_short: ""

abstract: One of the main questions regarding complex systems at large scales concerns the effective interactions and driving forces that emerge from the detailed microscopic properties. Coarse-grained models aim to describe complex systems in terms of coarse-scale equations with a reduced number of degrees of freedom. Recent developments in machine-learning algorithms have significantly empowered the discovery process of governing equations directly from data. However, it remains difficult to discover partial differential equations (PDEs) with high-order derivatives. In this paper, we present data-driven architectures based on a multilayer perceptron, a convolutional neural network (CNN), and a combination of a CNN and long short-term memory structures for discovering the nonlinear equations of motion for phase-field models with nonconserved and conserved order parameters. The well-known Allen-Cahn, Cahn-Hilliard, and phase-field crystal models were used as test cases. Two conceptually different types of implementations were used. (a) guided by physical intuition (such as the local dependence of the derivatives) and (b) in the absence of any physical assumptions (black-box model). We show that not only can we effectively learn the time derivatives of the field in both scenarios, but we can also use the data-driven PDEs to propagate the field in time and achieve results in good agreement with the original PDEs.

# Summary. An optional shortened abstract.
tags:
- Machine Learning
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/pre2022.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
