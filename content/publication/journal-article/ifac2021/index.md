---
title: "Periodicity Scoring of Time Series Encodes Dynamical Behavior of the Tumor Suppressor p53"
authors:
- Caroline Moosmüller
- Christopher J. Tralie
- admin 
- Zehor Belkhatir
- Maryam Pouryahya
- José Reyes
- Joseph O Deasy
- Allen R. Tannenbaum
- Ioannis G. Kevrekidis

date: "2021-01-01T00:00:00Z"
doi: "https://doi.org/10.1016/j.ifacol.2021.06.106"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "IFAC-PapersOnLine 54, no. 9 (2021): 488-495"
publication_short: ""

abstract: In this paper we analyze the dynamical behavior of the tumor suppressor protein p53, an essential player in the cellular stress response, which prevents a cell from dividing if severe DNA damage is present. When this response system is malfunctioning, e.g. due to mutations in p53, uncontrolled cell proliferation may lead to the development of cancer. Understanding the behavior of p53 is thus crucial to prevent its failing. It has been shown in various experiments that periodicity of the p53 signal is one of the main descriptors of its dynamics, and that its pulsing behavior (regular vs. spontaneous) indicates the level and type of cellular stress. In the present work, we introduce an algorithm to score the local periodicity of a given time series (such as the p53 signal), which we call Detrended Autocorrelation Periodicity Scoring (DAPS). It applies pitch detection (via autocorrelation) on sliding windows of the entire time series to describe the overall periodicity by a distribution of localized pitch scores. We apply DAPS to the p53 time series obtained from single cell experiments and establish a correlation between the periodicity scoring of a cell’s p53 signal and the number of cell division events. In particular, we show that high periodicity scoring of p53 is correlated to a low number of cell divisions and vice versa. We show similar results with a more computationally intensive state-of-the-art periodicity scoring algorithm based on topology known as Sw1PerS. This correlation has two major implications. It demonstrates that periodicity scoring of the p53 signal is a good descriptor for cellular stress, and it connects the high variability of p53 periodicity observed in cell populations to the variability in the number of cell division events.


# Summary. An optional shortened abstract.
tags:
- Computational Biology
- Machine Learning
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/ifac2021.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
