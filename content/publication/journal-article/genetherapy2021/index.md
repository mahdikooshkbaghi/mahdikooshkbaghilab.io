---
title: "Novel tool to quantify with single-cell resolution the number of incoming AAV genomes co-expressed in the mouse nervous system"
authors:
- Carola J. Maturana
- Jessica L. Verpeut
- admin 
- Esteban A. Engel
date: "2021-06-01T00:00:00Z"
doi: "https://doi.org/10.1038/s41434-021-00272-8"

# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Gene Therapy (2021): 1-6"
publication_short: ""

abstract: Adeno-associated viral (AAV) vectors are an established and safe gene delivery tool to target the nervous system. However, the payload capacity of <4.9 kb limits the transfer of large or multiple genes. Oversized payloads could be delivered by fragmenting the transgenes into separate AAV capsids that are then mixed. This strategy could increase the AAV cargo capacity to treat monogenic, polygenic diseases and comorbidities only if controlled co-expression of multiple AAV capsids is achieved on each transduced cell. We developed a tool to quantify the number of incoming AAV genomes that are co-expressed in the nervous system with single-cell resolution. By using an isogenic mix of three AAVs each expressing single fluorescent reporters, we determined that expression of much greater than 31 AAV genomes per neuron in vitro and 20 genomes per neuron in vivo is obtained across different brain regions including anterior cingulate, prefrontal, somatomotor and somatosensory cortex areas, and cerebellar lobule VI. Our results demonstrate that multiple AAV vectors containing different transgenes or transgene fragments, can efficiently co-express in the same neuron. This tool can be used to design and improve AAV-based interrogation of neuronal circuits, map brain connectivity, and treat genetic diseases affecting the nervous system.

# Summary. An optional shortened abstract.
tags:
- Computational Biology
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/GeneTherapy2021.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
