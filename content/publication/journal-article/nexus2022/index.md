---
title: "On the parameter combinations that matter and on those that do not: data-driven studies of parameter (non)identifiability"
authors:
- Nikolaos Evangelou
- Noah J. Wichrowski
- George A. Kevrekidis
- Felix Dietrich
- admin
- Sarah McFann
- Ioannis G. Kevrekidis
date: "2022-09-14T00:00:00Z"
doi: "https://doi.org/10.1093/pnasnexus/pgac154"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "PNAS Nexus (2022)"
publication_short: ""

abstract: We present a data-driven approach to characterizing nonidentifiability of a model’s parameters and illustrate it through dynamic as well as steady kinetic models. By employing Diffusion Maps and their extensions, we discover the minimal combinations of parameters required to characterize the output behavior of a chemical system; a set of effective parameters for the model. Furthermore, we introduce and use a Conformal Autoencoder Neural Network technique, as well as a kernel-based Jointly Smooth Function technique, to disentangle the redundant parameter combinations that do not affect the output behavior from the ones that do. We discuss the interpretability of our data-driven effective parameters, and demonstrate the utility of the approach both for behavior prediction and parameter estimation. In the latter task, it becomes important to describe level sets in parameter space that are consistent with a particular output behavior. We validate our approach on a model of multisite phosphorylation, where a reduced set of effective parameters (nonlinear combinations of the physical ones) has previously been established analytically.



# Summary. An optional shortened abstract.
tags:
- Machine Learning
- Model Reduction
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/nexus2022.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
