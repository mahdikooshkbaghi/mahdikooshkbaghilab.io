---
title: "Entropy production analysis for mechanism reduction"
authors:
- admin
- Christos E. Frouzakis
- Konstantinos Boulouchos
- Ilya V. Karlin
date: "2014-06-01T00:00:00Z"
doi: "https://doi.org/10.1016/j.combustflame.2013.12.016"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Combustion and flame 161, no. 6 (2014): 1507-1515"
publication_short: ""

abstract: A systematic approach based on the relative contribution of each elementary reaction to the total entropy production is developed for eliminating species from detailed reaction mechanisms in order to generate skeletal schemes. The approach is applied to a database of solutions for homogeneous constant pressure auto-ignition of n-heptane to construct two skeletal schemes for different threshold values defining the important reactions contributing to the total entropy production. The accuracy of the skeletal mechanisms is evaluated in spatially homogeneous systems for ignition delay time, a single-zone engine model, and a perfectly stirred reactor in a wide range of thermodynamic conditions. High accuracy is also demonstrated for the speed and structure of spatially-varying premixed laminar flames.

# Summary. An optional shortened abstract.
tags:
- Combustion
- Model Reduction
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/combustflame2014.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
