---
title: "Structural and mechanistic basis of σ-dependent transcriptional pausing"
authors:
- Chirangini Pukhrambam
- Vadim Molodtsov
- admin
- Ammar Tareen
- Hoa Vu
- Kyle S. Skalenko
- Min Su
- Zhou Yin
- Jared T. Winkelman
- Justin B. Kinney
- Richard H. Ebright
- Bryce E. Nickels
date: "2022-06-02T00:00:00Z"
doi: "https://doi.org/10.1073/pnas.2201301119"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Proceedings of the National Academy of Sciences 119 (23) (2022)"
publication_short: ""

abstract: In σ-dependent transcriptional pausing, the transcription initiation factor σ, translocating with RNA polymerase (RNAP), makes sequence-specific protein–DNA interactions with a promoter-like sequence element in the transcribed region, inducing pausing. It has been proposed that, in σ-dependent pausing, the RNAP active center can access off-pathway “backtracked” states that are substrates for the transcript-cleavage factors of the Gre family and on-pathway “scrunched” states that mediate pause escape. Here, using site-specific protein–DNA photocrosslinking to define positions of the RNAP trailing and leading edges and of σ relative to DNA at the λPR′ promoter, we show directly that σ-dependent pausing in the absence of GreB in vitro predominantly involves a state backtracked by 2–4 bp, and σ-dependent pausing in the presence of GreB in vitro and in vivo predominantly involves a state scrunched by 2–3 bp. Analogous experiments with a library of 47 (∼16,000) transcribed-region sequences show that the state scrunched by 2–3 bp—and only that state—is associated with the consensus sequence, T−3N−2Y−1G+1, (where −1 corresponds to the position of the RNA 3′ end), which is identical to the consensus for pausing in initial transcription and which is related to the consensus for pausing in transcription elongation. Experiments with heteroduplex templates show that sequence information at position T−3 resides in the DNA nontemplate strand. A cryoelectron microscopy structure of a complex engaged in σ-dependent pausing reveals positions of DNA scrunching on the DNA nontemplate and template strands and suggests that position T−3 of the consensus sequence exerts its effects by facilitating scrunching.
# Summary. An optional shortened abstract.
tags:
- Computational Biology
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/pnas_2022_1.pdf
url_code: 'https://github.com/jbkinney/21_nickels/'
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
