---
title: "Numerical simulation of atomic layer deposition for thin deposit formation in a mesoporous substrate"
authors:
- Liwei Zhuang
- Peter Corkery
- Dennis T. Lee
- Seungjoon Lee
- admin
- Zhen‐liang Xu
- Gance Dai
- Ioannis G. Kevrekidis
- Michael Tsapatsis
date: "2021-05-01T00:00:00Z"
doi: "https://doi.org/10.1002/aic.17305"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "AIChE Journal (2021): e17305"
publication_short: ""

abstract: ZnO deposition in porous γ-Al2O3 via atomic layer deposition (ALD) is the critical first step for the fabrication of zeolitic imidazolate framework membranes using the ligand-induced perm-selectivation process (Science, 361 (2018), 1008–1011). A detailed computational fluid dynamics (CFD) model of the ALD reactor is developed using a finite-volume-based code and validated. It accounts for the transport processes within the feeding system and reaction chamber. The simulated precursor spatiotemporal profiles assuming no ALD reaction were used as boundary conditions in modeling diethylzinc reaction/diffusion in porous γ-Al2O3, the predictions of which agreed with experimental electron microscopy measurements. Further simulations confirmed that the present deposition flux is much less than the upper limit of flux, below which the decoupling of reactor/substrate is an accurate assumption. The modeling approach demonstrated here allows for the design of ALD processes for thin-film membrane formation including the synthesis of metal–organic framework membranes.

# Summary. An optional shortened abstract.
tags:
- Fluid Mechanics
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/aiche2021.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
