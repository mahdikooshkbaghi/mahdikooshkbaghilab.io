---
title: "n-Heptane/air combustion in perfectly stirred reactors: Dynamics, bifurcations and dominant reactions at critical conditions"
authors:
- admin
- Christos E. Frouzakis
- Konstantinos Boulouchos
- Ilya V. Karlin
date: "2015-09-01T00:00:00Z"
doi: "https://doi.org/10.1016/j.combustflame.2015.05.002"

# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Combustion and Flame 162, no. 9 (2015): 3166-3179"
publication_short: ""

abstract: The dynamics of n-heptane/air mixtures in perfectly stirred reactors (PSR) is investigated systematically using bifurcation and stability analysis and time integration. A skeletal mechanism of n-heptane constructed by entropy production analysis is employed, which is extensively validated for different conditions with respect to the ignition delay time, laminar flame speed, and the typical hysteretic behavior observed in PSRs. The significantly reduced size of the skeletal mechanism, enables the extension of the bifurcation analysis to multiple parameters. In addition to residence time, the effect of equivalence ratio, volumetric heat loss and the simultaneous variation of residence time and inlet temperature on the reactor state are investigated using one- and two-parameter continuations. Multiple ignition and extinction turning points leading to steady state multiplicity and oscillatory behavior of both the strongly burning and the cool flames are found, which can lead to oscillatory (dynamic) extinction. The two-parameter continuations revealed isolas and codimension-two bifurcations (cusp, Bogdanov–Takens, and double Hopf). Computational Singular Perturbation (CSP) and entropy production analysis were used to probe the complex kinetics at interesting points of the bifurcation diagrams.

# Summary. An optional shortened abstract.
tags:
- Combustion
- Dynamical Systems
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/combustflame2015.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
