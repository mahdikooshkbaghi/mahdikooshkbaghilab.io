---
title: "Non-perturbative hydrodynamic limits: A case study"
authors:
- Ilya V. Karlin
- Shyam S. Chikatamarla
- admin
date: "2014-06-01T00:00:00Z"
doi: "https://doi.org/10.1016/j.physa.2014.02.018"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Physica A: Statistical Mechanics and its Applications 403 (2014): 189-194"
publication_short: ""

abstract: We introduce non-perturbative analytical techniques for the derivation of the hydrodynamic manifolds from kinetic equations. The new approach is analogous to the Schwinger–Dyson equation of quantum field theories, and its derivation is demonstrated with the construction of the exact diffusion manifold for a model kinetic equation.

# Summary. An optional shortened abstract.
tags:
- Physical Kinetics
- Model Reduction
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/physa2014.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
