---
title: "Spectral quasi-equilibrium manifold for chemical kinetics"
authors:
- admin
- Christos E. Frouzakis
- Konstantinos Boulouchos
- Ilya V. Karlin
date: "2016-04-01T00:00:00Z"
doi: "https://doi.org/10.1021/acs.jpca.6b01709"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "The Journal of Physical Chemistry A 120, no. 20 (2016): 3406-3413"
publication_short: ""

abstract: The Spectral Quasi-Equilibrium Manifold (SQEM) method is a model reduction technique for chemical kinetics based on entropy maximization under constraints built by the slowest eigenvectors at equilibrium. The method is revisited here and discussed and validated through the Michaelis–Menten kinetic scheme, and the quality of the reduction is related to the temporal evolution and the gap between eigenvalues. SQEM is then applied to detailed reaction mechanisms for the homogeneous combustion of hydrogen, syngas, and methane mixtures with air in adiabatic constant pressure reactors. The system states computed using SQEM are compared with those obtained by direct integration of the detailed mechanism, and good agreement between the reduced and the detailed descriptions is demonstrated. The SQEM reduced model of hydrogen/air combustion is also compared with another similar technique, the Rate-Controlled Constrained-Equilibrium (RCCE). For the same number of representative variables, SQEM is found to provide a more accurate description.

# Summary. An optional shortened abstract.
tags:
- Combustion
- Model Reduction
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/jphyschema2016.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
