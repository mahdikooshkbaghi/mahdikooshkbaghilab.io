---
title: "Generalized entropy production analysis for mechanism reduction"
authors:
- Luigi Acampora
- admin
- Christos E. Frouzakis
- Francesco S. Marra
date: "2019-09-01T00:00:00Z"
doi: "https://doi.org/10.1080/13647830.2018.1504990"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Combustion Theory and Modelling 23, no. 2 (2019): 197-209"
publication_short: ""

abstract: The paper introduces a generalized formulation for the computation of the relative contribution of each elementary reaction to the total entropy production, which has been proposed as a measure of the importance of elementary reactions and used for the reduction of detailed chemical reaction mechanisms. The reduction method is extended for the cases where the principle of detailed balance does not hold or apply, namely in the case of irreversible reactions or when the reverse rate constants are not computed via the thermodynamic equilibrium constants. Using a mechanism for n-butane consisting exclusively of reversible reactions, the new formulation is compared to the original one, and then applied for the construction of a skeletal mechanism for n-dodecane starting from a detailed mechanism which includes predominantly irreversible reactions. The skeletal scheme is found to accurately capture the ignition delay times over an extended range of pressure, initial temperature and equivalence ratio, the steady-state temperature as function of the residence time in a non-isothermal adiabatic perfectly stirred reactor, and the laminar flame speed of atmospheric flames at different unburned mixture temperatures and equivalence ratios.

# Summary. An optional shortened abstract.
tags:
- Combustion
- Model Reduction
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/ctm2019.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
