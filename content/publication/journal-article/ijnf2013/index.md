---
title: "A collocated grid, projection method for time‐accurate calculation of low‐Mach number variable density flows in general curvilinear coordinates"
authors:
- admin
- Bamdad Lessani
date: "2013-05-01T00:00:00Z"
doi: "https://doi.org/10.1002/fld.3734"
# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "International Journal for Numerical Methods in Fluids 72, no. 3 (2013): 301-319"
publication_short: ""

abstract: A time-accurate algorithm is proposed for low-Mach number, variable density flows on curvilinear grids. Spatial discretization is performed on collocated grid that offers computational simplicity in curvilinear coordinates. The flux interpolation technique is used to avoid the pressure odd–even decoupling of the collocated grid arrangement. To increase the stability of the method, a two-step predictor–corrector time integration scheme is employed. At each step, the projection method is used to calculate the hydrodynamic pressure and to satisfy the continuity equation. The robustness and accuracy of the method is illustrated with a series of numerical experiments including thermally driven cavity, polar cavity, three-dimensional cavity, and direct numerical simulation of non-isothermal turbulent channel flow.

# Summary. An optional shortened abstract.
tags:
- Fluid Mechanics
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: uploads/ijnf2013.pdf
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
