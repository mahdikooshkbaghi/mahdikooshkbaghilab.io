module github.com/wowchemy/starter-academic/exampleSite

go 1.15

require (
	github.com/wowchemy/wowchemy-hugo-modules/v5 v5.3.0
	github.com/wowchemy/wowchemy-hugo-modules/wowchemy-cms/v5 v5.0.0-20210912231654-d054640701c4 // indirect
	github.com/wowchemy/wowchemy-hugo-modules/wowchemy/v5 v5.0.0-20210912231654-d054640701c4 // indirect
)
