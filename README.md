# Mahdi Kooshkbaghi Personal Webpage

This folder contains the source code of my personal webpage. I have used [Hugo](https://gohugo.io/) framework to build the website together with the [Academic Theme](https://academic-demo.netlify.app/).
